# Hugo Bootswatch

A bootswatch theme for hugo.

For the complete changelog, see [changelog.md](changelog.md)


## Usage

1. checkout this repo
3. `git submodule init`
4. `git submodule update`
5. `cd submodules/bootswatch/`
6. `git checkout v5.3.1`
7. `cd ../lightbox2/`
8. `git checkout v2.11.4`
9. `cd ../../`
10. unfortunately, *hugo* cannot work with symlinks in the css folder, so we have to copy the submodule files there manually. I hope, sometimes symlinks will work (or another solution)
	Additionally, bootswatch does not provide means to eliminate remote google fonts without sass or less.
	Thus the according code will be deleted from the css files until a better solution is available.
	1. `./copy-submodule-files`

## Images

Images can be used out-of-the-box as header images or in a gallery.

The theme uses the description EXIF tag if present.
It generates a warning for images without description, so you can find these images easier.

You can read all tags and set the description e.g. with [exiftool](https://exiftool.org)

~~~ bash
$ exiftool 2023-05-14_berlin-malmoe-stockholm-1.jpg
  ...
$ exiftool -overwrite_original -exif:ImageDescription="Herr Ringelhorn schaut aus dem Zugfenster am Gesundbrunnen." 2023-05-14_berlin-malmoe-stockholm-1.jpg
  ...
  1 image files updated
$ exiftool 2023-05-14_berlin-malmoe-stockholm-1.jpg
  ...
  Image Description               : Herr Ringelhorn schaut aus dem Zugfenster am Gesundbrunnen.
  ...
$
~~~


## Other Software

### hugo

- <https://gohugo.io>
- [hugo docker image](https://hub.docker.com/r/ekleinod/hugo)

*hugo* is the static website generator I use.

### Bootswatch, Bootstrap, Fontawesome, Lightbox2

- <https://bootswatch.com>
	- Bootswatch provides several themes for bootstrap websites.
- <https://getbootstrap.com>
	- Bootstrap is a frontend toolkit for responsive websites.
- <https://fontawesome.com>
	- Fontawesome provides icons for websites.
- <https://lokeshdhakar.com/projects/lightbox2/>
	- Lightbox provides image overlay instead of opening images on their own.

### Beautiful Hugo

- <https://github.com/halogenica/beautifulhugo>
  - [MIT-License](https://raw.githubusercontent.com/halogenica/beautifulhugo/master/LICENSE)

I used Beautiful Hugo as starting point and inspiration for the code and the structure of the code.
While I reqrote most of the code I can recommend Beautiful Hugo for it's clear structure and well written code:


## Copyright, license

(c) copyright 2022-2023, Ekkart Kleinod, ekleinod@edgesoft.de

Hugo Bootswatch is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
