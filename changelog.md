# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [Unreleased]

### Added

- watched date, conclusion, IMDB link and score in meta data #17 #18
- new shortcode `image` #15
- max count for tag cloud #19
- new parameter for setting which content type is included in the rss feed #23

### Changes

- partial `image` to `imagetag` #15
- switched `page` to `default` in order to have a default fallback for headers, footers etc. #20

### Fixed

- wrong context in call of search page #16
- formatting of blockquotes #14
- changelog links
- removed static pages from rss feed #23
- restored header and footer for static pages #20


## [1.0.0] - 2023-09-01

- initial version #1

### Added

- hugo theme
- basic functionality for blogs and static pages
- issue tracker for further development


[Unreleased]: https://gitlab.com/ekleinod/hugo-bootswatch/-/compare/1.0.0...HEAD
[1.0.0]: https://gitlab.com/ekleinod/hugo-bootswatch/-/tags/1.0.0

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
