/*
 * hugo bootswatch theme, search
 */

const thbParams = new URLSearchParams(window.location.search);
const thbQuery = thbParams.has('query') ? thbParams.get('query') : "";
const thbStarredQuery = (thbSearchAutostarQuery) ? "*" + thbQuery + "*" : thbQuery;

document.getElementById('search-input-nav').value = thbQuery;
document.getElementById('search-input-searchpage').value = thbQuery;

const thbSearchResults = document.getElementById('search-results')
const thbSearchResultsInfo = document.getElementById('search-results-info')

function handleErrors(response) {
	if (!response.ok) {
		throw Error(response.statusText);
	}
	return response;
}

async function search(searchIndexURL, searchItem) {

	fetch(searchIndexURL)
		.then(handleErrors)
		.then((response) => response.json())
		.then((json) => {
			const idx = lunr(function () {

				// search fields
				this.ref('id');
				this.field('title', {
					boost: 15
				});
				this.field('subtitle');
				this.field('content', {
					boost: 10
				});

				Object.entries(json).forEach(([id, values]) => {
					this.add({
						id: id,
						title: values.title,
						subtitle: values.subtitle,
						content: values.content,
						uri: values.uri
					});
				});

			});

			const results = idx.search(searchItem);
			thbSearchResultsInfo.textContent = `${thbPluralize(results.length, thbSearchResultsMatchesSingular, thbSearchResultsMatchesPlural)}.`;
			if (results.length) {

				thbSearchResultsInfo.classList.add("alert-success");

				const template = document.getElementById("search-result-item");

				for (const n in results) {
					const item = json[results[n].ref];

					var element = template.content.cloneNode(true);
					element.querySelector(".result-item-content").href = item.uri;
					element.querySelector(".result-item-title").textContent = item.title;
					element.querySelector(".result-item-summary").textContent = item.content.substring(0, 150) + "...";
					thbSearchResults.appendChild(element);
				}

			}

		})
		.catch(err => {
			thbSearchResultsInfo.textContent = `${thbSearchResultsError}: ${err}.`;
			thbSearchResultsInfo.classList.add("alert-warning");
		})
	;

}

if (thbQuery) {
	search(thbSearchIndex, thbStarredQuery);
} else {
	thbSearchResultsInfo.textContent = tbhSearchResultsEmptyQuery;
}
