/*
 * hugo bootswatch theme
 */

// lightbox
lightbox.option({
	'albumLabel': "Bild %1 von %2",
	'wrapAround': true
})

// internationalization
const thbPluralRules = new Intl.PluralRules('en-US');

function thbPluralize(count, singular, plural) {
  const grammaticalNumber = thbPluralRules.select(count);
  switch (grammaticalNumber) {
    case "one":
      return `${count} ${singular}`;
    case "other":
      return `${count} ${plural}`;
    default:
      throw new Error('Unknown: '+grammaticalNumber);
  }
}
